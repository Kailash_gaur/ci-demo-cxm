# Implementation - baseversion Default #

This repository represents a default bare configuration for cxm implementations.

### What is this repository for? ###

* Any new implementations should be based of this version.  To setup a new client have an admin fork this project.

### Directory Structure ###

* Jenkinsfile - Jenkins build script to cause a server to be built on commit.
* Cloudfile.j2 - Used to configure the implementation
* contributers.txt - whomever worked on this project
  * config/ - files copied to jboss folder
  * data/ - files and folders copied to /opt/cxm/data/
    * CXMBaseFolder/
    * CXMReports/
    * email/
    * htmlFileBaseFolder/
  * sql/
    * cxmdb/
      * changesets/ - liqubase changesets to run
      starterdb.sql - starter database

*If not using the starter db (ie export from windows db) be sure to do a search and replace on DEFINERS.  All definer should be set to 'cxm'@'localhost'.  Typically these are the problem definers.  'cxm'@'%' or 'root'@'%' or 'root'@'localhost' *

Handy way to check for deiner in bash.
> grep -iR definer *.sql

### How do I get set up? ###

* Summary of set up
  * setup ssh/git: https://confluence.atlassian.com/bitbucket/set-up-ssh-for-git-728138079.html
  * install atom or other text editor
  * clone this project


## What guidelines do I need to follow? ##

* Metadata changes
  * metadata changes are managed through changelogs
  * see /sql/cxmdb/changelog for examples.
  * changelogs are cummulative across versions.
  * until a cxm_client_version is incremented all metadata changes should happen in the same db.chagelog-_version_.xml file.
  * see http://www.liquibase.org/documentation/changes/
* Writing tests
  * All implementations should have tests scenarios written in HipTest for each customization
  * Define Test Runs for each deliverable phase.  Be sure to include any core functionality
    * alpha
    * beta
    * acceptance
* Code review
  * Policy TBD
* Versioning
  * new implementations start at version 0.1.0
  * beta version is 0.8.0
  * acceptance version is 0.9.0
  * go live version is 1.0.0
*

### How do I configure an implementation?

**Consider using git flow process if multiple developers will be working on the project. See http://danielkummer.github.io/git-flow-cheatsheet/**

These instructions assume a less complex development process / single developer.

    mkdir -p ~/workspaces/clients/
    cd ~/workspaces/clients/
    git clone git@bitbucket.org:creedenz/ci-<project>.git
    cd ci-<project>
    git checkout dev

* Choose a starter database or use the one included in the template.  
  * replace the database in /sql/cxmdb/
* Configure Cloudfile.j2
  * base_hostname=_acme.cxmweb.com_
  * cxm_app_version=_3.4.2.0_
  * cxm_app_war_filename=_CreedenzCXM-3.4.2.0-LS.war_
  * cxm_client=_acme_
* add files to CXMBaseFolder
* add files to CXMReports
* add files to email
* add files to htmlFileBaseFolder
* add tests to /tests/
* edit config/application.properties
* commit changes to local git as appropriate.  
* git push your commits.

Pushing your commits to bitbucket will cause a server to be created with you configuration.  The following is the general naming convention:

* Development Branch: dev-<project>.cxmweb.com
* Feature Branch: feature-<featurename>-<project>.cxmweb.com
* QA Branch: qa-<project>.cxmweb.com
* Production Branch: <project>.cxmweb.com

You should get a message in Slack or in your email when the server is ready.  Generally under 8 minutes for inactive servers and under 3 minutes for active servers

### What happens when I push a commit to bitbucket?
A jenkins job will get kicked off.

This will result in a server being built based on the branch that was committed.

Examples:
* Dev Server: dev-hostname.cxmweb.com
* QA Server: qa-hostname.cxmweb.com
* Prod Server: hostname.cxmweb.com

** Be sure to deprovision dev and qa servers when they are done.**

### How do I connect to the database with MySQL Workbench or DBForge?

#### Prerequisites:
* Provide Atul with your ssh public key
* Your ssh public key is installed on the server

GOTCHA: since we use dynamic hosts for dev and qa servers, when a server is rebuilt, new ssh connections to the same hostname will  fail.  

SOLUTION: Disable Strict Host Key Checking
* Windows: Edit C:\Users\myname\AppData\Roaming\MySQL\Workbench\ssh\config
* OSX: Edit ~\ssh\config
* Add the following
      Host *.cxmweb.com
        StrictHostKeyChecking no
        UserKnownHostsFile=/dev/null

#### Instructions for MySQL Workbench:
* Start MySQL Workbench
* Add Connection
  * Connection Name: _dev-template.cxmweb.com_
  * Connection Method: **Standard TCP/IP over SSH**
  * Parameters:
    * SSH Hostname: _dev-template.cxmweb.com**:22**_
    * SSH Key File: _full path to your ssh key file_
    * Username: **cxm**
    * Password: **goflyaK1t3**
    * Default Schema: **cxmdb**
  * Advanced: (optional)
    * Use compression protocol: **checked**
* Test Connection.. OK

NOTE: We decided to include password here since we restrict access to server by individualized ssh keys


  #### Instructions for MySQL DBForge:
  * Start DBForge for MySQL
  * Add Connection
    * General
      * Host: localhost
      * Port: 3306
      * Username: **cxm**
      * Password: **goflyaK1t3**
      * Database: **cxmdb**
    * Security
      * Host: _dev-template.cxmweb.com_
      * Port: 22
      * User: cxm
      * Passphrase: ~empty~
      * Private Key File: _full path to your ssh key file_
  * Test Connection.. OK

  NOTE: We decided to include password here since we restrict access to server by individualized ssh keys
